package dto

import "time"

type ForumRequestResponse struct {
	Id          int       `json:"id"`
	UserId      int       `json:"user_id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type ForumResponse struct {
	Id          int       `json:"id"`
	CreatedBy   string    `json:"created_by"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
}

type ForumAllResponse struct {
	ForumList  []ForumResponse `json:"forum_list"`
	TotalForum int             `json:"total_forum"`
}

type ForumLike struct {
	UserId  int `json:"user_id"`
	ForumId int `json:"forum_id"`
}

type ForumDetailResponse struct {
	Id           int               `json:"id"`
	Title        string            `json:"title"`
	Description  string            `json:"description"`
	CreatedBy    string            `json:"created_by"`
	TotalComment int               `json:"total_comment"`
	TotalLikes   int               `json:"total_likes"`
	CreatedAt    time.Time         `json:"created_at"`
	CommentList  []CommentResponse `json:"comment_list"`
}
