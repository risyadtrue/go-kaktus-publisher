package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go-kaktus-publisher/controller/dto"
	"go-kaktus-publisher/controller/publish"
	"go-kaktus-publisher/helper"
	"net/http"
	"strconv"
	"time"
)

const (
	forumCommentQueueName = "forumComment_queue"
)

func PublishCreateComment(writer http.ResponseWriter, request *http.Request) {
	var commentRequest dto.CommentRequest
	err := json.NewDecoder(request.Body).Decode(&commentRequest)

	forumId := chi.URLParam(request, "forumId")
	forumIdConv, err := strconv.Atoi(forumId)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	userId := request.Context().Value("userId").(int)

	commentRequest.ForumId = forumIdConv
	commentRequest.UserId = userId
	commentRequest.CreatedAt = time.Now()
	commentRequest.UpdatedAt = time.Now()

	consumeCommentData, err := publish.PublishData(commentRequest, forumCommentQueueName)
	err = json.Unmarshal(consumeCommentData, &commentRequest)

	helper.HttpResponseSuccess(writer, request, commentRequest)
}

func PublishCreateReplyComment(writer http.ResponseWriter, request *http.Request) {
	var commentReplyRequest dto.CommentRequest
	err := json.NewDecoder(request.Body).Decode(&commentReplyRequest)

	forumId := chi.URLParam(request, "forumId")
	commentId := chi.URLParam(request, "commentId")
	forumIdConv, _ := strconv.Atoi(forumId)
	commentIdConv, _ := strconv.Atoi(commentId)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	userId := request.Context().Value("userId").(int)

	commentReplyRequest.ForumId = forumIdConv
	commentReplyRequest.ParentId = commentIdConv
	commentReplyRequest.UserId = userId
	commentReplyRequest.CreatedAt = time.Now()

	consumeCommentReplyData, err := publish.PublishData(commentReplyRequest, forumCommentQueueName)
	err = json.Unmarshal(consumeCommentReplyData, &commentReplyRequest)

	helper.HttpResponseSuccess(writer, request, commentReplyRequest)
}
