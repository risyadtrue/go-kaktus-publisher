package controller

import (
	"encoding/json"
	"fmt"
	"go-kaktus-publisher/controller/dto"
	"go-kaktus-publisher/controller/publish"
	"go-kaktus-publisher/helper"
	"go-kaktus-publisher/utils"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

const (
	userQueueName      = "userData_queue"
	userLoginQueueName = "userLoginData_queue"
)

func PublishCreateUser(writer http.ResponseWriter, request *http.Request) {
	var userRequest dto.UserRequest
	var userResponse dto.UserResponse
	err := json.NewDecoder(request.Body).Decode(&userRequest)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	userRequest.CreatedAt = time.Now()
	userRequest.UpdatedAt = time.Now()

	consumeDataUser, err := publish.PublishData(userRequest, userQueueName)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(consumeDataUser, &userResponse)

	token, err := utils.GenerateAuthToken(userResponse.Id)
	if err != nil {
		fmt.Println("lol")
		helper.HttpResponseError(writer, request, nil, http.StatusBadRequest)
		return
	}

	userResponse.Token = token

	helper.HttpResponseSuccess(writer, request, userResponse)
}

func PublishLogin(writer http.ResponseWriter, request *http.Request) {
	var userRequest dto.UserLoginRequest
	var userResponse dto.UserResponse
	err := json.NewDecoder(request.Body).Decode(&userRequest)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	consumeDataLogin, err := publish.PublishData(userRequest, userLoginQueueName)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(consumeDataLogin, &userResponse)

	if userResponse.Id == 0 {
		helper.HttpResponseError(writer, request, nil, http.StatusNotFound)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(userResponse.Password), []byte(userRequest.Password))
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	token, err := utils.GenerateAuthToken(userResponse.Id)
	if err != nil {
		fmt.Println("lol")
		helper.HttpResponseError(writer, request, nil, http.StatusBadRequest)
		return
	}

	userResponse.Token = token

	helper.HttpResponseSuccess(writer, request, userResponse)
}
