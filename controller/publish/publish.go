package publish

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"go-kaktus-publisher/connection"
	"math/rand"
)

func randomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

// publish data from client to server
func PublishData(data interface{}, queueName string) (consumeData []byte, err error) {
	ch, err := connection.RabbitMQConnection()
	if err != nil {
		return
	}

	// declare queue callback
	q, err := ch.QueueDeclare("", false, false, true, false, nil)
	if err != nil {
		return
	}

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		return
	}

	// (byte) marshal to publish
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return
	}

	corrId := randomString(32)

	// publish request
	err = ch.Publish("", queueName, false, false, amqp.Publishing{
		ContentType:   "text/plain",
		CorrelationId: corrId,
		ReplyTo:       q.Name,
		Body:          dataBytes,
	})
	if err != nil {
		return
	}

	// consume callback
	for d := range msgs {
		if corrId == d.CorrelationId {
			consumeData = d.Body
		}
		break
	}

	return
}

// publish data from client to server
func PublishGetData(queueName string) (consumeData []byte, err error) {
	ch, err := connection.RabbitMQConnection()
	if err != nil {
		return
	}

	// declare queue callback
	q, err := ch.QueueDeclare("", false, false, true, false, nil)
	if err != nil {
		return
	}

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		return
	}

	corrId := randomString(32)

	// publish request
	err = ch.Publish("", queueName, false, false, amqp.Publishing{
		ContentType:   "text/plain",
		CorrelationId: corrId,
		ReplyTo:       q.Name,
	})
	if err != nil {
		return
	}

	// consume callback
	for d := range msgs {
		if corrId == d.CorrelationId {
			consumeData = d.Body
		}
		break
	}

	return
}
