package controller

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go-kaktus-publisher/controller/dto"
	"go-kaktus-publisher/controller/publish"
	"go-kaktus-publisher/helper"
	"net/http"
	"strconv"
	"time"
)

const (
	forumQueueName         = "forumData_queue"
	getForumQueueName      = "getForumData_queue"
	UserForumQueueName     = "getUserForumData_queue"
	userForumLIkeQueueName = "userForumLike_queue"
	ForumDetailQueueName   = "getForumDetailData_queue"
)

func PublishCreateForum(writer http.ResponseWriter, request *http.Request) {
	var forumRequest, forumResponse dto.ForumRequestResponse
	err := json.NewDecoder(request.Body).Decode(&forumRequest)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	userId := request.Context().Value("userId").(int)

	forumRequest.UserId = userId
	forumRequest.CreatedAt = time.Now()
	forumRequest.UpdatedAt = time.Now()

	consumeDataForum, err := publish.PublishData(forumRequest, forumQueueName)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(consumeDataForum, &forumResponse)

	helper.HttpResponseSuccess(writer, request, forumResponse)
}

func PublishGetForums(writer http.ResponseWriter, request *http.Request) {
	// change to slice of forum (later)
	var forumDataResponse dto.ForumAllResponse
	consumeAllDataForum, err := publish.PublishGetData(getForumQueueName)
	err = json.Unmarshal(consumeAllDataForum, &forumDataResponse)

	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(writer, request, forumDataResponse)
}

func PublishGetDetailForum(writer http.ResponseWriter, request *http.Request) {
	var forumDetailResponse dto.ForumDetailResponse
	forumId := chi.URLParam(request, "forumId")
	forumIdConv, err := strconv.Atoi(forumId)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	consumeForumDetailData, err := publish.PublishData(forumIdConv, ForumDetailQueueName)
	err = json.Unmarshal(consumeForumDetailData, &forumDetailResponse)
	helper.HttpResponseSuccess(writer, request, forumDetailResponse)
}

func PublishGetUserForums(writer http.ResponseWriter, request *http.Request) {
	userId := chi.URLParam(request, "userId")
	userIdConv, _ := strconv.Atoi(userId)
	var forumDataResponse []dto.ForumResponse

	consumeUserForumData, err := publish.PublishData(userIdConv, UserForumQueueName)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(consumeUserForumData, &forumDataResponse)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	helper.HttpResponseSuccess(writer, request, forumDataResponse)

}

func PublishLikeForum(writer http.ResponseWriter, request *http.Request) {
	forumId := chi.URLParam(request, "forumId")
	var ForumRequest dto.ForumLike
	forumIdConv, err := strconv.Atoi(forumId)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	userId := request.Context().Value("userId").(int)

	userForumLike := dto.ForumLike{
		UserId:  userId,
		ForumId: forumIdConv,
	}

	consumeDataLike, err := publish.PublishData(userForumLike, userForumLIkeQueueName)
	err = json.Unmarshal(consumeDataLike, &ForumRequest)

	helper.HttpResponseSuccess(writer, request, ForumRequest)
}
