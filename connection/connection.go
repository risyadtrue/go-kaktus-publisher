package connection

import (
	"fmt"
	"github.com/streadway/amqp"
)

func RabbitMQConnection() (ch *amqp.Channel, err error) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		fmt.Println("cannot connect RabbitMQ")
	}
	//defer conn.Close()

	ch, err = conn.Channel()
	if err != nil {
		fmt.Println("error make channel")
	}
	//defer ch.Close()

	return
}
