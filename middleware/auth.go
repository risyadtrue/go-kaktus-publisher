package middleware

import (
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"go-kaktus-publisher/helper"
	"net/http"
	"strings"
)

var SECRET_KEY = []byte("LogkarManiaMantap")

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			helper.HttpResponseError(writer, request, nil, http.StatusUnauthorized)
			return
		}

		tokenSplit := strings.Split(authHeader, " ")
		tokenString := tokenSplit[1]

		token, err := validateToken(tokenString)
		if err != nil {
			helper.HttpResponseError(writer, request, nil, http.StatusUnauthorized)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			helper.HttpResponseError(writer, request, nil, http.StatusUnauthorized)
			return
		}

		userId := int(claim["userId"].(float64))

		ctxValue := context.WithValue(request.Context(), "userId", userId)
		next.ServeHTTP(writer, request.WithContext(ctxValue))
	})
}

func validateToken(token string) (*jwt.Token, error) {
	tkn, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)

		if !ok {
			return nil, errors.New("Invalid token")
		}

		return []byte(SECRET_KEY), nil
	})

	if err != nil {
		return tkn, nil
	}

	return tkn, nil
}
