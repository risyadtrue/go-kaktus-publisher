package routes

import (
	"fmt"
	"github.com/go-chi/chi"
	"go-kaktus-publisher/controller"
	"go-kaktus-publisher/middleware"
	"net/http"
)

func RoutesCollection() {
	r := chi.NewRouter()
	r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("Hello from publisher"))
	})

	r.Route("/api/v1", func(r chi.Router) {
		r.Post("/user/register", controller.PublishCreateUser)
		r.Post("/user/login", controller.PublishLogin)
		r.With(middleware.AuthMiddleware).Post("/forum", controller.PublishCreateForum)
		r.Get("/forum", controller.PublishGetForums)
		r.Get("/user/{userId}/forum", controller.PublishGetUserForums)
		r.With(middleware.AuthMiddleware).Post("/forum/{forumId}/like", controller.PublishLikeForum)
		r.Get("/forum/{forumId}", controller.PublishGetDetailForum)
		r.With(middleware.AuthMiddleware).Post("/comment/{forumId}", controller.PublishCreateComment)
		r.With(middleware.AuthMiddleware).Post("/comment/{forumId}/reply/{commentId}", controller.PublishCreateReplyComment)
	})

	fmt.Println("Server running on localhost:3000")
	http.ListenAndServe(":3000", r)
}
