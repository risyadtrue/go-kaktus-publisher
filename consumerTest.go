package main

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

func FailOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

const (
	forumQueueName            = "forumData_queue"
	getForumQueueName         = "getForumData_queue"
	getUserForumQueueName     = "getUserForumData_queue"
	userForumLIkeQueueName    = "userForumLike_queue"
	ForumDetailQueueName      = "getForumDetailData_queue"
	forumCommentQueueName     = "forumComment_queue"
	forumCommentRelyQueueName = "forumCommentReply_queue"
)

func main() {
	// server yang consume

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(forumCommentRelyQueueName, false, false, true, false, nil)
	FailOnError(err, "failed to declare queue")

	err = ch.Qos(1, 0, false)
	FailOnError(err, "failed to set Qos")

	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)

	foreverChannel := make(chan bool)

	// goroutine
	go func() {
		for d := range msgs {
			data := string(d.Body)
			fmt.Println(data)

			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          d.Body,
			})
			FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request (from client)")
	<-foreverChannel
}
