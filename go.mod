module go-kaktus-publisher

go 1.17

require github.com/go-chi/chi v1.5.4

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/streadway/amqp v1.0.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)
