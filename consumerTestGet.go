package main

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"time"
)

type DummyForum struct {
	Id          int       `json:"id"`
	UserId      int       `json:"user_id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	// server yang consume

	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare("getForumData_queue", false, false, true, false, nil)
	failOnError(err, "failed to declare queue")

	err = ch.Qos(1, 0, false)
	failOnError(err, "failed to set Qos")

	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)

	forumData := DummyForum{
		Id:          1,
		UserId:      1,
		Title:       "Cek from consumer",
		Description: "Consumer for your life!!",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	forumDataBytes, err := json.Marshal(forumData)

	foreverChannel := make(chan bool)

	// goroutine
	go func() {
		for d := range msgs {
			data := string(d.Body)
			fmt.Println(data)

			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          forumDataBytes,
			})
			failOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request (from client)")
	<-foreverChannel
}
