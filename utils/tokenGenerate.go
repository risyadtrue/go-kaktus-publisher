package utils

import "github.com/dgrijalva/jwt-go"

var SECRET_KEY = []byte("LogkarManiaMantap")

func GenerateAuthToken(userId int) (string, error) {
	claim := jwt.MapClaims{}

	// add to ken payload
	claim["userId"] = userId

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	signedToken, err := token.SignedString(SECRET_KEY)
	if err != nil {
		return signedToken, err
	}

	return signedToken, nil
}
